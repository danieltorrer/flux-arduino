#include <SPI.h>
#include <Ethernet.h>
#include <NewPing.h>

#define TRIGGER_PIN_1  9
#define ECHO_PIN_1     8

#define TRIGGER_PIN_2  7
#define ECHO_PIN_2     6

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress server(104, 197, 0, 122);
EthernetClient client;
long distancia1;
long distancia2;
long tiempo;
long dis;
NewPing sensor1(TRIGGER_PIN_1, ECHO_PIN_1, MAX_SENSOR_DISTANCE);
NewPing sensor2(TRIGGER_PIN_2, ECHO_PIN_2, MAX_SENSOR_DISTANCE);

void setup() 
{
  Serial.begin(9600);
  while(Ethernet.begin(mac) == 0);
  delay(1000);
  tiempo = sensor1.ping_median();
  distancia1 = tiempo / US_ROUNDTRIP_CM;
  delay(10);
  tiempo = sensor2.ping_median();
  distancia2 = tiempo / US_ROUNDTRIP_CM;
  Serial.print("Distancia 1: ");
  Serial.println(distancia1);
  Serial.print("Distancia 2: ");
  Serial.println(distancia2);
}

void loop() 
{
  tiempo = sensor1.ping_median();
  dis = tiempo / US_ROUNDTRIP_CM;
  Serial.print("Entrada : ");
  Serial.println(dis);
  if(dis < (distancia1 - 3))
  {
    entrada();
    delay(1000);
  }
  tiempo = sensor2.ping_median();
  dis = tiempo / US_ROUNDTRIP_CM;
  Serial.print("Salida : ");
  Serial.println(dis);
  if(dis < (distancia2 - 3))
  {
    salida();
    delay(1000);
  }
}

void entrada()
{
  client.stop();
  if(client.connect(server,80))
  {
    client.println("POST /entrada HTTP/1.1");
    client.println("Host: 104.197.0.122");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();
  }
}

void salida()
{
  client.stop();
  if(client.connect(server,80))
  {
    client.println("POST /salida HTTP/1.1");
    client.println("Host: 104.197.0.122");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();
  }
}



